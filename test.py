import os
from collections import OrderedDict
from torch.autograd import Variable
from options.test_options import TestOptions
from data.data_loader import CreateDataLoader
from models.models import create_model
import util.util as util
from util.visualizer import Visualizer
from util import html
import torch

###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################        
def result2chord2(song_id, out_path='./results/chord2label/test_latest/images', result_dir='./results/chord2label/test_latest/images'):   
    ###########################################################################
    # Step1. 將一首歌曲的預測結果圖，水平合併成一張 256(長)x 歌曲長度(寬) x 3(RGB)
    ###########################################################################
    import time as ts
    import cv2
    from os import path
    import numpy as np
    startTime = ts.time()
    startTotalTime = ts.time()
    img_index = 1
    input_data = []
    synthesized_data = []
    while True:
        input_file_path = '%s/%d_%d_input_label.png'%(result_dir,song_id,img_index)
        synthesized_file_path = '%s/%d_%d_synthesized_image.png'%(result_dir,song_id,img_index)
        if path.exists(input_file_path) == True and path.exists(synthesized_file_path) == True:
            inputs = cv2.imread(input_file_path)
            synthesized = cv2.imread(synthesized_file_path)
            if img_index == 1:
                input_data = inputs
                synthesized_data = synthesized
            else:
                input_data = np.hstack((input_data, inputs))
                synthesized_data = np.hstack((synthesized_data, synthesized))
        elif path.exists(input_file_path) == False and path.exists(synthesized_file_path) == False and img_index == 1:
            print('song_id:%d not exist!!!'%(song_id))
            return []
        else:
            break
        img_index += 1 

    input_all_path = '%s/%s_input_all.png'%(out_path,song_id)
    synthesized_all_path = '%s/%s_synthesized_all.png'%(out_path,song_id)
    cv2.imwrite(input_all_path, input_data) 
    cv2.imwrite(synthesized_all_path, synthesized_data) 
    print(input_data.shape, synthesized_data.shape)
    print('[result2chord] Done ... %.3fs'%(ts.time() - startTime)) 
    info_dict={
        'input_path':os.path.basename(input_all_path),
        'input_shape':input_data.shape,
        'synthesized_path':os.path.basename(synthesized_all_path),
        'synthesized_shape':synthesized_data.shape,
    }   
    return info_dict

opt = TestOptions().parse(save=False)
opt.nThreads = 1   # test code only supports nThreads = 1
opt.batchSize = 1  # test code only supports batchSize = 1
opt.serial_batches = True  # no shuffle
opt.no_flip = True  # no flip

data_loader = CreateDataLoader(opt)
dataset = data_loader.load_data()
visualizer = Visualizer(opt)
# create website
web_dir = os.path.join(opt.results_dir, opt.name, '%s_%s' % (opt.phase, opt.which_epoch))
webpage = html.HTML(web_dir, 'Experiment = %s, Phase = %s, Epoch = %s' % (opt.name, opt.phase, opt.which_epoch))

# test
if not opt.engine and not opt.onnx:
    model = create_model(opt)
    if opt.data_type == 16:
        model.half()
    elif opt.data_type == 8:
        model.type(torch.uint8)
            
    if opt.verbose:
        print(model)
else:
    from run_engine import run_trt_engine, run_onnx
    
song_id_list = []
for i, data in enumerate(dataset):
    if i >= opt.how_many:
        break
    if opt.data_type == 16:
        data['label'] = data['label'].half()
        data['inst']  = data['inst'].half()
    elif opt.data_type == 8:
        data['label'] = data['label'].uint8()
        data['inst']  = data['inst'].uint8()
    if opt.export_onnx:
        print ("Exporting to ONNX: ", opt.export_onnx)
        assert opt.export_onnx.endswith("onnx"), "Export model file should end with .onnx"
        torch.onnx.export(model, [data['label'], data['inst']],
                          opt.export_onnx, verbose=True)
        exit(0)
    minibatch = 1 
    if opt.engine:
        generated = run_trt_engine(opt.engine, minibatch, [data['label'], data['inst']])
    elif opt.onnx:
        generated = run_onnx(opt.onnx, opt.data_type, minibatch, [data['label'], data['inst']])
    else:        
        generated = model.inference(data['label'], data['inst'], data['image'])
        
    visuals = OrderedDict([('input_label', util.tensor2label(data['label'][0], opt.label_nc)),
                           ('synthesized_image', util.tensor2im(generated.data[0]))])
    img_path = data['path']
    img_path_str = os.path.basename(str(img_path)).split('_', 2)
    song_id_list.append(img_path_str[0])
    
    print('process image... %s' % (img_path))
    visualizer.save_images(webpage, visuals, img_path)

for i in list(set(song_id_list)):
    info_dict = result2chord2(song_id=int(i),out_path='%s/images'%(web_dir), result_dir='%s/images'%(web_dir))
    webpage.add_images([info_dict['input_path']], [info_dict['input_path']], [info_dict['input_path']], width=info_dict['input_shape'][1])
    webpage.add_images([info_dict['synthesized_path']], [info_dict['synthesized_path']], [info_dict['synthesized_path']], width=info_dict['synthesized_shape'][1])
webpage.save()
