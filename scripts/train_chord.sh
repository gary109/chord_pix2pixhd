set -ex


##########
# COMMON #
##########
CPU='--gpu_ids 0 '
# BATCH_SIZE='--batchSize 2 '
BATCH_SIZE='--batchSize 24 '
# NTHREADS='--nThreads 2 '
NTHREADS='--nThreads 8 '
NFG='--ngf 64 '
NO_FLIP='--no_flip '
# NO_FLIP=' '
RESIZE_OR_CROP='--resize_or_crop none '
DATAROOT='--dataroot ./datasets/chords/ '
USE_DROPOUT='--use_dropout '
NO_INSTANCE='--no_instance '
NAME='--name chord2label '
NITER='--niter 50 '
NITER_DECAY='--niter_decay 5' 

# TF_LOG='--tf_log '
# CONTINUE_TRAIN='--continue_train '
CONTINUE_TRAIN=' '
LABEL_NC='--label_nc 0 '
NORM='--norm batch '
LOADSIZE='--loadSize 256 '
FINESIZE='--fineSize 256 '
DISPLAY_WINSIZE='--display_winsize 256 '
SAVE_EPOCH_FREQ='--save_epoch_freq 1 '
SAVE_LATEST_FREQ='--save_latest_freq 5000 '
CHECKPOINTS='--checkpoints_dir ./checkpoint'

# NETG='--netG local '
NETG='--netG global '

# command
python ./train.py \
  ${NAME} \
  ${CHECKPOINTS} \
  ${DATAROOT} \
  ${USE_DROPOUT} \
  ${CPU} \
  ${NFG} \
  ${NO_INSTANCE} \
  ${BATCH_SIZE} \
  ${NO_FLIP} \
  ${LABEL_NC} \
  ${NORM} \
  ${LOADSIZE} \
  ${FINESIZE} \
  ${DISPLAY_WINSIZE} \
  ${SAVE_EPOCH_FREQ} \
  ${SAVE_LATEST_FREQ} \
  ${NITER} \
  ${NITER_DECAY} \
  ${NTHREADS} \
  ${RESIZE_OR_CROP} \
  # ${TF_LOG} \
  # ${CONTINUE_TRAIN} \
