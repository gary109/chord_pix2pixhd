set -ex
###################
# Single CPU 0    #
###################
CPU='--gpu_ids -1 '
BATCH_SIZE='--batchSize 1 '

###################
# Single GPU 0    #
###################
# CPU='--gpu_ids 0 '
# BATCH_SIZE='--batchSize 1 '

###################
# Multi GPU 0,1  #
###################
# CPU='--gpu_ids 0,1 '
# BATCH_SIZE='--batchSize 2 '

##########
# COMMON #
##########
NFG='--ngf 64 '
NO_FLIP='--no_flip '
RESIZE_OR_CROP='--resize_or_crop none '
DATAROOT='--dataroot ./datasets/chords/ '
USE_DROPOUT='--use_dropout '
NO_INSTANCE='--no_instance '
NAME='--name chord2label '
NITER='--niter 100 '
NITER_DECAY='--niter_decay 100' 
NTHREADS='--nThreads 0 '
TF_LOG='--tf_log '
CONTINUE_TRAIN='--continue_train '
LABEL_NC='--label_nc 0 '
NORM='--norm batch '
LOADSIZE='--loadSize 256 '
FINESIZE='--fineSize 256 '
DISPLAY_WINSIZE='--display_winsize 256 '
SAVE_EPOCH_FREQ='--save_epoch_freq 100 '
# NETG='--netG local '
NETG='--netG global '
HOW_MANY='--how_many 1000'
WHICH_EPOCH='--which_epoch latest'
# WHICH_EPOCH='--which_epoch 17'


# command
python ./test.py \
  ${CPU} \
  ${NAME} \
  ${USE_DROPOUT} \
  ${DATAROOT} \
  ${NO_INSTANCE} \
  ${BATCH_SIZE} \
  ${LABEL_NC} \
  ${NORM} \
  ${LOADSIZE} \
  ${DISPLAY_WINSIZE} \
  ${RESIZE_OR_CROP} \
  ${NFG} \
  ${NETG} \
  ${FINESIZE} \
  ${HOW_MANY} \
  ${WHICH_EPOCH} \
  
